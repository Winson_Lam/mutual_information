import numpy as np
from sklearn.feature_extraction.text import CountVectorizer as cv
import pandas as pd
import scipy

X_train = np.array([['Nathan', 'Winson', 'Robin', 'Ally', 'Richard'], 
        ['Winson', 'Markus', 'James', 'Gareth', 'Robin'],
        ['Nathan', 'Ally', 'Winson', 'Gareth', 'Robin'],
        ['Ally', 'Winson', 'Gareth', 'Robin', 'Markus'],
        ['Markus', 'Gareth', 'Robin', 'James', 'Ally'],
        ['Winson', 'Ally', 'Gareth', 'Richard', 'James'],
        ['Nathan', 'Ally', 'Richard', 'Markus', 'Winson'], 
        ['Ally', 'Gareth', 'James', 'Winson', 'Markus'],
        ['Richard', 'Gareth', 'Winson', 'Markus', 'Robin'], 
         ['Winson', 'Nathan', 'Ally', 'Richard', 'Robin'],
         ['Richard', 'Ally', 'Winson', 'Robin', 'Markus'],
         ['Gareth', 'Markus', 'Robin', 'Ally', 'Richard']])

y_train = np.array(['won', 'lose', 'won', 'won', 'lose', 'lose', 'won', 'draw', 'draw', 'won', 'draw', 'draw'])

def mI(N_11, N_01, N_10, N_00):
    """
    N_11 : TrueTrue
    N_01 : FalseTrue
    N_10 : TrueFalse
    N_00 : FalseFalse
    """
    N = N_11 + N_01 + N_10 + N_00
    
    I = N_11/N * np.log((N * N_11) / ((N_11 + N_10) * (N_11 + N_01))) + \
        N_01/N * np.log((N * N_01) / ((N_01 + N_11) * (N_01 + N_00))) + \
        N_10/N * np.log((N * N_10) / ((N_10 + N_00) * (N_10 + N_11))) + \
        N_00/N * np.log((N * N_00) / ((N_00 + N_10) * (N_00 + N_01))) 
        
    return I

def digitalisation(X_train, y_train):
    # Convert X into 0 to m, y from 0 to n
    # Reason for digitalising is because usual matrices are numerical
    X_digital = np.zeros(X_train.shape)
    y_digital = np.zeros(y_train.shape)
    map_categoryX = dict(zip(np.unique(X_train), range(len(np.unique(X_train)))))
    map_categoryy = dict(zip(np.unique(y_train), range(len(np.unique(y_train)))))

    for i in range(len(X_train)):
        row = X_train[i]
        for j in range(len(row)):
            k = row[j]
            X_digital[i, j] = map_categoryX[k]

    for i in range(len(y_train)):
        row = y_train[i]
        y_digital[i] = map_categoryy[row]

    return X_digital, y_digital, map_categoryX, map_categoryy

X_digital, y_digital, map_categoryX, map_categoryy = digitalisation(X_train, y_train)

def calculate_mI_forEach_class(X_digital, y_digital, map_categoryX, map_categoryy):    
    mI_matrix = np.zeros([len(map_categoryX), len(map_categoryy)])
    for i in range(len(np.unique(y_train))):
        classe = i
        for j in np.unique(X_digital):
            feature = j
            withFeatureWithClass = np.finfo(np.float32).eps # approx 0
            withFeatureNoClass = np.finfo(np.float32).eps # approx 0
            noFeatureWithClass = np.finfo(np.float32).eps # approx 0
            noFeatureNoClass = np.finfo(np.float32).eps # approx 0

            for k in range(len(X_digital)):
                if feature in X_digital[k] and classe == y_digital[k]:
                    withFeatureWithClass += 1
                if feature in X_digital[k] and classe != y_digital[k]:
                    withFeatureNoClass += 1
                if feature not in X_digital[k] and classe == y_digital[k]:
                    noFeatureWithClass += 1
                if feature not in X_digital[k] and classe != y_digital[k]:
                    noFeatureNoClass += 1
            mutual_info = mI(withFeatureWithClass, withFeatureNoClass, noFeatureWithClass, noFeatureNoClass)
            mI_matrix[int(j), i] = mutual_info
    return mI_matrix

mI_matrix = calculate_mI_forEach_class(X_digital, y_digital, map_categoryX, map_categoryy)

pd.DataFrame(mI_matrix, index=map_categoryX.keys(), columns=map_categoryy.keys()).round(4)

mI_matrix.round(3)